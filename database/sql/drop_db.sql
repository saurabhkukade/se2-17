drop table if exists label_details cascade ;
drop table if exists label_assignment cascade ;
drop table if exists label_info cascade ;

drop table if exists msg_in_box cascade ;
drop table if exists msg_sent_box cascade ;
drop table if exists msg_attachments cascade ;
drop table if exists msgs cascade ;

drop table if exists group_members cascade ;
drop table if exists group_info cascade ;
drop table if exists user_info cascade ;
drop table if exists access_level cascade ;
drop table if exists messeges cascade ;
