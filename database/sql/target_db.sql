drop table intermediate;

create table intermediate
       (
       request_id varchar primary key,
       session_id varchar not null,
       request varchar not null,
       response varchar default null
       );
