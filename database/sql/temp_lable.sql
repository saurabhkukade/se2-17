drop table user_msg_lable cascade;
drop table user_lable cascade;
create table user_lable (
       lable_id serial primary key,
       lable_name varchar not null,
       user_id varchar references user_info(user_id),
       parent_id integer references user_lable(lable_id),
       from_sender varchar,
       msg_contains varchar
);


create table user_msg_lable (
       msg_id varchar references messages(msg_id),
       user_id varchar references user_info(user_id),
       lable_id integer references user_lable(lable_id)
);

insert into user_lable (lable_id ,lable_name,user_id,parent_id,from_sender)values(0,'lable','saurabh',null,'saurabh');
insert into user_lable (lable_name,user_id,parent_id,from_sender)values('my_lable','saurabh',0,'saurabh');
