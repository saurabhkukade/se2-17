drop table if exists user_msg_lable cascade ;
drop table if exists user_lable cascade ;
drop table if exists subscription cascade ;
drop table if exists services cascade ;
drop table if exists messages cascade ;
drop table if exists user_info cascade ;

create table if not exists user_info(
       user_id varchar primary key,
       user_name varchar not null,
       gender varchar,
       last_name varchar,
       mobile_number varchar unique,
       email_address varchar unique not null,
       date_of_birth varchar,
       allocated_size int default 0,
       password varchar not null check(length(password)<18)
);

CREATE TABLE IF NOT EXISTS messages(
    msg_id varchar primary key,
    contains varchar,
    sender_id varchar references user_info(user_id),
    receiver_id varchar references user_info(user_id)
);

create table services
       (
	service_name varchar primary key
	);
create table subscription
       (
	user_id varchar references user_info (user_id),
	service varchar references services (service_name),
	primary key (user_id, service)
);
create table user_lable (
       user_id varchar references user_info(user_id),
       lable varchar,
       primary key(user_id, lable)
);

create table user_msg_lable (
       user_id varchar references user_info (user_id),
       msg_id varchar references messages (msg_id),
       lable varchar,
       primary key(user_id, msg_id, lable)
);

insert into user_info (user_id,user_name,email_address,password) values('admin','System Admin','admin@messanger.com','123');
insert into services (service_name)values('lable'),('test case');
