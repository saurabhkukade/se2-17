drop table if exists label_details cascade ;
drop table if exists label_assignment cascade ;
drop table if exists label_info cascade ;

drop table if exists msg_in_box cascade ;
drop table if exists msg_sent_box cascade ;
drop table if exists msg_attachments cascade ;
drop table if exists msgs cascade ;

drop table if exists group_members cascade ;
drop table if exists group_info cascade ;
drop table if exists messages cascade ;
drop table if exists user_info cascade ;
drop table if exists access_level cascade ;


create table if not exists access_level(
       access_id varchar(10) primary key check(access_id like 'a%'),
       permission varchar(10) check(length(permission) <10) not null,
       details varchar
);
create table if not exists user_info(
       user_id varchar primary key,
       user_name varchar not null,
       gender varchar,
       last_name varchar,
       mobile_number varchar unique,
       email_address varchar unique not null,
       date_of_birth varchar,
       allocated_size int default 0,
       password varchar not null check(length(password)<18)
);


CREATE TABLE IF NOT EXISTS messages(
    msg_id varchar primary key,
    contains varchar,
    sender_id varchar references user_info(user_id),
    receiver_id varchar references user_info(user_id)
);


create table if not exists group_info(
       group_id varchar primary key check(group_id like 'g%'),
       grout_name varchar,
       member_count int default 1,
       group_owner varchar,
       foreign key(group_owner) references user_info(user_id)
);

create table if not exists group_members(
       group_id varchar,
       member_id varchar,
       access_id varchar(10),
       foreign key(group_id) references group_info(group_id),
       foreign key(access_id) references access_level(access_id)
);

create table if not exists msgs(
       msg_id varchar primary key check(msg_id like 'm%'),
       msg_details varchar
);

create table if not exists msg_sent_box(
       receiver_id varchar,
       sent_time varchar,
       msg_id varchar,
       sender_id varchar primary key,
       foreign key(msg_id) references msgs(msg_id),
       foreign key(sender_id) references user_info(user_id),
       foreign key(receiver_id) references user_info(user_id)      
);

create table if not exists msg_in_box(
       msg_id varchar,
       sender_id varchar,
       receiver_id varchar primary key,
       received_time varchar,
       foreign key(msg_id) references msgs(msg_id),
       foreign key(sender_id) references user_info(user_id),
       foreign key(receiver_id) references user_info(user_id)
);

create table if not exists msg_attachments(
       msg_id varchar,
       attachment_id varchar primary key,
       attachment_type varchar,
       attachment_containt varchar,
       check(attachment_type = 'image' or attachment_type = 'doc' or attachment_type = 'text'),
       foreign key(msg_id) references msgs(msg_id)
);
--------------------------

create table if not exists label_info(
       label_id varchar check(label_id like 'l%') primary key,
       label_name varchar not null,
       user_id varchar,
       foreign key(user_id) references user_info(user_id)
);

create table if not exists label_details(
       label_id varchar primary key,
       parrent_label varchar,
       label_name varchar not null,
       foreign key(label_id) references label_info(label_id),
       foreign key(parrent_label) references label_info(label_id)
);

create table if not exists label_assignment(
	label_id varchar primary key,
	msg_id varchar,
	foreign key(msg_id) references msgs(msg_id),
    foreign key(label_id) references label_info(label_id)
);
