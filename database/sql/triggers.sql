drop trigger if exists admin_insertion;
drop trigger if exists group_count;
drop trigger if exists delete_group_from_group_members;
drop trigger if exists delete_user_from_group_members;
drop trigger if exists delete_user_from_group_info;
drop trigger if exists delete_user_from_sent_msg;
drop trigger if exists delete_user_from_received_msg;
drop trigger if exists delete_user_from_sent_msg_re;
drop trigger if exists delete_user_from_received_msg_re;
drop trigger if exists delete_user_lable_assignment;
drop trigger if exists delete_lable_details;
drop trigger if exists delete_lable_assignment;
drop trigger if exists delete_msg;

-----triggers for any change in user_id
----------------------------------------------------------------------------------------------------

create trigger delete_user_from_group_members after delete on user_info
		when((select count() from group_members where (old.user_id = group_members.member_id)) > 0)
		begin
			delete from group_members where old.user_id = group_members.member_id;
		end;
create trigger delete_user_from_group_info after delete on user_info
		when((select count() from group_info where (old.user_id = group_info.group_owner)) > 0)
		begin	
			delete from group_info where old.user_id = group_info.member_id;
		end;
---
create trigger delete_user_from_sent_msg after delete on user_info
		when((select count() from msg_sent_box where old.user_id = msg_sent_box.sender_id ) > 0)
		begin	
			delete from msg_sent_box where old.user_id = msg_sent_box.sender_id;
		end;
create trigger delete_user_from_sent_msg_re after delete on user_info
		when((select count() from msg_sent_box where old.user_id = msg_sent_box.receiver_id ) > 0)
		begin	
			delete from msg_sent_box where old.user_id = msg_sent_box.receiver_id;
		end;
---
create trigger delete_user_from_received_msg_re after delete on user_info		
		when((select count(*) from msg_in_box where old.user_id = msg_in_box.receiver_id) > 0)
		begin
			delete from msg_in_box where old.user_id = msg_in_box.receiver_id;
		end;
				
create trigger delete_user_from_received_msg after delete on user_info
		when((select count(*) from msg_in_box where old.user_id = msg_in_box.sender_id) > 0)
		begin
			delete from msg_in_box where old.user_id = msg_in_box.sender_id;
		end;
---
create trigger delete_user_lable_assignment after delete on user_info		
		when((select count(*) from label_assignment where old.user_id = label_assignment.user_id) > 0)
		begin
			delete from label_assignment where old.user_id = label_assignment.user_id;
		end;

-----------------------------------------------------------------------------------------------------
create trigger admin_insertion after insert on group_info
       for each row
       begin
       insert into group_members(group_id,member_id,access_id)values(new.group_id,new.group_owner,'a1111');
       end;

create trigger group_count after insert on group_members
       for each row 
       begin
       update group_info set member_count = member_count+1 where(new.group_id = group_info.group_id);
       end;

create trigger delete_group_from_group_members after delete on group_info
		when((select count(*) from group_info where old.group_id = group_members.group_id) > 0)
		begin
			delete from group_members where old.group_id = group_members.group_id;
		end;

create trigger delete_lable_assignment after delete on label_info
		begin
		delete from label_assignment where old.label_id = label_assignment.label_id;
		end;
			
create trigger delete_lable_details after delete on label_info
		when((select count(*) from group_info where old.label_id = label_details.label_id) > 0)	
		begin
			delete from group_members where old.label_id = label_details.label_id;
		end;

create trigger delete_msg after delete on msgs
		when((select count(*) from label_assignment where old.msg_id = label_assignment.msg_id) > 0)
		begin
			delete from label_assignment where old.msg_id = label_assignment.msg_id;
		end;
