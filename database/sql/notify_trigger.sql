drop TRIGGER emp_stamp on intermediate cascade;
drop FUNCTION emp_stamp() cascade;
CREATE FUNCTION emp_stamp() RETURNS trigger AS $emp_stamp$
    BEGIN
 	   NOTIFY dbchange;
	   RETURN NULL;
    END;
$emp_stamp$ LANGUAGE plpgsql;

CREATE TRIGGER emp_stamp AFTER INSERT OR UPDATE ON intermediate
    FOR EACH ROW EXECUTE PROCEDURE emp_stamp();
