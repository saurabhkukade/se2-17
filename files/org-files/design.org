* Processes
** Watcher
   - Scan the target-dir for new files
   - if new files found
     then
         process them with priorty
	 if (same priority)
	 then
	     tie breaker is file size.
	     
*** process files
    put the file contents in tempdb as file-name, contents, prioroity

** Validator
   - Scan new entry in tempdb.
   - if new entry found then get file-contents priority and process it.
   - Set the flag-validator in tempdb

** Compressor
   - Scan new entry in tempdb.
   - if new entry found then get file-contents according to priority and process it.
   - Set the flag-Compressor in tempdb
     
*** Processing:
    - Create its md5 hash of file contents. 
    - Take file contents and compress it tar.gz format.
    - Put compressed tarball and its md5 hash in tempdb-tgz-md5 database.

*** tempdb-tgz-md5 schema:
   | file-name-priority-timestamp | file-content-tarball | file-content-md5 |
    


* temdb
** schema
   | file-name-priority-timestamp | file-content | flag-compressor | flag-validator |
   |                              |              |                 |                |

** Actions
   + Set respective flag by compressor and validator once it processed
   + Remove record once both compressor and validator flag is set
   
