import psycopg2
import ConfigParser
import ast
import time
import logging


class Watcher():
    def __init__(self, conf_file):
        self.conf_file = conf_file
        self.read_config()
        logging.basicConfig(filename=self.log_file, level=logging.DEBUG, format='%(asctime)s : %(levelname)s : %(message)s')

    def read_config(self):
        config = ConfigParser.ConfigParser()
        config.read(self.conf_file)
        self.target_database = config.get('config', 'target_database')
        self.app_database = config.get('config', 'app_database')
        self.database_user = config.get('config', 'user')
        self.database_password = config.get('config', 'password')
        self.database_host = config.get('config', 'host')
        self.database_port = config.get('config', 'port')
        self.log_file = config.get('config', 'log_file')

    def get_database_connection(self, database):
        """ Returns database connection """
        if (database == "target_database"):
            database_name = self.target_database
        else:
            database_name = self.app_database
        try:
            con = psycopg2.connect(database=database_name, user=self.database_user, password=self.database_password, host=self.database_host, port=self.database_port)
            return con
        except Exception as e:
            return e, "connection", "failed"

    def validate_login(self, req,req_id):
        """ Check valid login request """
        password = req['password'][1]
        user_id = req['user_name'][1]
        logging.info("login_request: request_id = " + req_id + ",username = " + user_id + ", password = "+password)
        con = self.get_database_connection("app_database")
        cur = con.cursor()
        cmd = "select count(*) from user_info where user_id = '"+user_id+"' and password='"+password+"';"
        try:
            cur.execute("select count(*) from user_info where user_id = '"+user_id+"' and password='"+password+"';")
            containts = cur.fetchall()
            if(1 == containts[0][0]):
                con.close()
                logging.info("login_request: request_id = " + req_id + ", response = success")
                return self.serve_response("success",req_id)
            else:
                con.close()
                logging.info("login_request: request_id = " + req_id + ", response = invalid")
                return self.serve_response("Invalid",req_id)
        except Exception as e:
            con.close()
            return self.serve_response(e,req_id)

    def watch_database(self):
        """ check new entries for serving request """
        try:
            con = self.get_database_connection("target_database")
            cmd = "select * from intermediate where response is null ;"
            cur = con.cursor()
            cur.execute(cmd)
            output = cur.fetchall()
            con.close()
            if(output != []):
                logging.info("new_request: "+str(output))
            return output
        except Exception as e:
            con.close()
            return e,"Request"," failed"
        return "login_request"

    def serve_response(self, response, req_id):
        """ update the column in the target_database with response """
        try:
            con = self.get_database_connection("target_database")
            cmd = "update intermediate set response = '"+str(response)+"' where request_id = '"+req_id+"';"
            cur = con.cursor()
            cur.execute(cmd)
            con.commit()
            con.close()
            return
        except Exception as e:
            print e
            con.close()
            return e,"Response serve"," failed"

    def validate_registration(self, req,req_id):
        """ check the registration form is valid and make entry in user_info table """
        user_id = req["user_name"][1]
        logging.info("registration_request: request_id = " + req_id + ", request = "+str(req))
        con = self.get_database_connection("app_database")
        cur = con.cursor()
        col_lst = []
        val_lst = []
        for e in req:
            if(e != "pageName" and e!="config"):
                col_lst.append(e)
                val_lst.append(req[e][1])
        cmd = "insert into user_info ("+ ", ".join(col_lst)+") values ('"+"', '".join(val_lst) + "');"
        logging.info("registration_request: request_id = " + req_id + ", query = "+cmd)
        try:
            cur.execute(cmd)
            con.commit()
            con.close()
            self.serve_response("success",req_id)
            logging.info("registration_request: request_id = " + req_id + ", query = "+cmd+" response = success")
            return "success"
        except Exception as e:
            con.close()
            self.serve_response(e,req_id)
            logging.info("registration_request: request_id = " + req_id + ", query = "+cmd+" response = "+str(e))
            return e

    def send_msg(self, req,req_id):
        """ make entries of the new msgs """
        logging.info("send_msg_request: request_id = " + req_id + ", request = "+str(req))
        con = self.get_database_connection("app_database");
        cur = con.cursor()
        cmd = "select count(*) from user_info where user_id = '"+req["receiver_id"][1]+"';"
        logging.info("send_msg_request: request_id = " + req_id + ", query = "+cmd)
        cur.execute(cmd)
        containts = cur.fetchall()
        if(containts[0][0]!=1):
                logging.info("send_msg_request: request_id = " + req_id + ", response = invalid reciever")
                return self.serve_response("Invalid",req_id)
        lst = [str(time.ctime()),req["content"][1],req["sender_id"][1],req["receiver_id"][1]]
        cmd = "insert into messages(msg_id,contains,sender_id,receiver_id) values('"+"', '".join(lst)+"');"
        logging.info("send_msg_request: request_id = " + req_id + ", query = "+cmd)
        try:
            cur.execute(cmd)
            con.commit()
            con.close()
            logging.info("send_msg_request: request_id = " + req_id + ", response = success")
            return self.serve_response("success",req_id)
        except Exception as e:
            con.close()
            logging.info("send_msg_request: request_id = " + req_id + ", response = "+str(e))
            return self.serve_response(e, req_id)

    def make_ls(self, ls, delem):
        xs = []
        for msg in ls:
            xs.append(reduce((lambda x, y: x + delem + y), [x for x in msg]))
        xs = "#".join(xs)
        return xs

    def display_inbox(self, req, req_id):
        """ render inbox function """
        logging.info("inbox_request:request_id = " + req_id + ", request = " + str(req))
        con = self.get_database_connection("app_database")
        cmd = "select * from messages where receiver_id = '" + req["sender_id"][1] + "';"
        logging.info("inbox_request:request_id =" + req_id + ", query= " + cmd)
        try:
            cursor = con.cursor()
            cursor.execute(cmd)
            ls = cursor.fetchall()
            con.close()
            ls = self.make_ls(ls, "**")
            logging.info("inbox_request: request_id = " + req_id + ", response = "+str(ls))
            self.serve_response(ls, req_id)
        except Exception as e:
            con.close()
            logging.info("inbox_request: request_id = " + req_id + ", response = "+str(e))
            self.serve_response(e, req_id)

    def toggle_service(self,req, req_id):

        if req['status'][1] == 'Subscribe':
            cmd = "insert into subscription (user_id, service) values ('"+req["sender_id"][1]+"','"+req["service"][1]+"');"
            response = "service_set!" + req["service"][1]
        else:
            cmd = "delete from subscription where user_id = '"+req["sender_id"][1]+"' and service ='"+req["service"][1]+"';"
            response = "service_unset!" + req["service"][1]

        logging.info("service_request: request_id = " + req_id + ", request = "+str(req))
        con = self.get_database_connection("app_database")
        cur = con.cursor()
        try:
            cur.execute(cmd)
            con.commit()
            con.close()
            self.serve_response(response, req_id)
            logging.info("service_request: request_id = " + req_id + ", query = "+cmd+" response = success")
        except Exception as e:
            con.close()
            self.serve_response(e, req_id)
            logging.info("service_request: request_id = " + req_id + ", query = "+cmd+" response = "+str(e))

    def get_service_list(self, req, req_id):
        logging.info("service_list_request: request_id = " + req_id + ", request = "+str(req))
        con = self.get_database_connection("app_database")
        cmd1 = "select service_name from services;"
        cmd2 = "select service from subscription where user_id = '"+req["sender_id"][1]+"';"
        logging.info("service_list_request: request_id = " + req_id + ", query = "+cmd1)
        try:
            cursor = con.cursor()
            cursor.execute(cmd1)
            ls = cursor.fetchall()
            cursor.execute(cmd2)
            ps = cursor.fetchall()
            con.close()
            ls = self.make_ls(ls, "**")
            ps = self.make_ls(ps, "**")
            ls = 'services!' + ls + "!" + ps
            logging.info("service_list_request: request_id = " + req_id + ", response = "+str(ls))
            self.serve_response(ls, req_id)
        except Exception as e:
            con.close()
            logging.info("service_list_reques: request_id = " + req_id + ", response = "+str(e))
            self.serve_response(e, req_id)

    def get_parent_lable_id(self, lable_name, user_id):
        cmd = "select lable_id from user_lable where user_id = '"+user_id+"' and lable_name = '"+lable_name+"';"
        try:
            con = self.get_database_connection("app_database")
            cursor = con.cursor()
            cursor.execute(cmd)
            ls = cursor.fetchall()
            return str(ls[0][0])
        except Exception as e:
            print e
            return "0"

    def add_new_lable(self, req, req_id):
        lable_data = map(lambda x:x.split('#'),(req['add_lable'][1])[:-1].split("#"))
        lable_data = map(lambda x:((x[0].split('!!'))[0],(x[0].split('!!'))[1]),lable_data)
        lable_dict = dict(lable_data)
        if(lable_dict["parent_lable"] != ""):
            parent_lable = self.get_parent_lable_id(lable_dict["parent_lable"],req["sender_id"][1])
            lable_dict["parent_lable"] = parent_lable
        else:
            lable_dict["parent_lable"] = "0"
        logging.info("add_new_lable_request: request_id = " + req_id + ", request = "+str(req))
        cmd = "insert into user_lable (lable_name,user_id,parent_id,from_sender,msg_contains) values\
        ('"+lable_dict['lable_name']+"','"+req['sender_id'][1]+"','"+lable_dict['parent_lable']+"','"+lable_dict['from_lable_rule']+"','"+lable_dict['contains_lable_rule']+"');"
        con = self.get_database_connection("app_database");
        cur = con.cursor()
        try:
            cur.execute(cmd)
            con.commit()
            con.close()
            self.serve_response('lable_add!' + lable_dict['lable_name'], req_id)
            logging.info("new_lable_request: request_id = " + req_id + ", query = "+cmd+" response = success")
        except Exception as e:
            print e
            con.close()
            self.serve_response("lable_add!failed", req_id)
            logging.info("new_lable_request: request_id = " + req_id + ", query = "+cmd+" response = "+str(e))

    def get_lable_list(self, id):
        if(id != "null"):
            id = "= " + str(id)
        else:
            id = "is " + id
        try:
            con = self.get_database_connection("app_database")
            cmd = "SELECT lable_id, lable_name FROM user_lable WHERE parent_id "+id+" ;"
            cur = con.cursor()
            cur.execute(cmd)
            res = cur.fetchall()
            return res
        except Exception as e:
            print e

    def generate_dict(self, lable, lable_dict):
        lable_lst = self.get_lable_list(lable[0])
        lable_dict[lable[1]] = lable_lst
        for i in lable_lst:
            self.generate_dict(i, lable_dict)

    def get_user_lables(self, req, req_id):
        lable_dict = {}
        self.generate_dict((0, "lable"), lable_dict)
        ls = str(lable_dict).replace("'", "#")
        # ls = self.make_ls(lable_dict,"**")
        ls = "user_lable_list!" + ls
        self.serve_response(ls, req_id)
        return

    def apply_lable(self, req, req_id):
        logging.info("apply_lable_request: request_id = " + req_id + ", request = "+str(req))
        cmd = "insert into user_msg_lable(user_id, msg_id, lable) values ('"+req['sender_id'][1]+"','"+req['msg_id'][1]+"','"+req['new_lable'][1]+"');"
        con = self.get_database_connection("app_database")
        cur = con.cursor()
        try:
            cur.execute(cmd)
            con.commit()
            con.close()
            self.serve_response('lable_apply!' + req['new_lable'][1], req_id)
            logging.info("apply_lable_request: request_id = " + req_id + ", query = "+cmd+" response = success")
        except Exception as e:
            print e
            con.close()
            self.serve_response("lable_apply!failed", req_id)
            logging.info("apply_lable_request: request_id = " + req_id + ", query = "+cmd+" response = "+str(e))

    def fetch_msg_by_lable(self, lable, sender_id):
        cmd = "select from_sender,msg_contains from user_lable where user_id = '"+sender_id+"' and lable_name = '"+lable+"';"
        try:
            con = self.get_database_connection("app_database")
            cursor = con.cursor()
            cursor.execute(cmd)
            ls = cursor.fetchall()
            return ls[0]
        except Exception as e:
            print e
            return "0"

    def get_msg_on_rule(self, rule, sender_id):
        from_rule = rule[0]
        containts = rule[1]
        if(containts is not None):
            containts = " contains like '%" + containts + "%'"
        if(from_rule != 'NULL'):
            frm_cmd = ""
            for i in from_rule.split(","):
                frm_cmd += "sender_id = '" + i + "' or "
        else:
            frm_cmd = ""
        rule_cmd = frm_cmd + containts
        cmd = "select * from messages where receiver_id = '" + sender_id + "' and (" + rule_cmd + " );"
        try:
            con = self.get_database_connection("app_database")
            cursor = con.cursor()
            cursor.execute(cmd)
            ls = cursor.fetchall()
            return ls
        except Exception as e:
            print e
            return "0"

    def search_msgs(self, req, req_id):
        """ render inbox after searching lable function """
        lable_dict = {}
        self.generate_dict((req['search_lable_id'][1], req['search_lable'][1]), lable_dict)
        root_lable = req['search_lable'][1]
        lable_set = set()

        def iterate_dict(lable):
            ls = lable_dict[lable]
            lable_set.add(lable)
            for i in ls:
                lable_set.add(i[1])
                iterate_dict(i[1])
        iterate_dict(root_lable)

        lable_msg_ls = []
        for lable in lable_set:
            rule = self.fetch_msg_by_lable(lable, req['sender_id'][1])
            lable_msg_ls += (self.get_msg_on_rule(rule, req['sender_id'][1]))
        ls = list(set(lable_msg_ls))
        ls = self.make_ls(ls, "**")
        logging.info("inbox_request: request_id = " + req_id + ", response = " + str(ls))
        self.serve_response("lable_inbox!" + ls, req_id)

    def function_api(self, req, req_id):
        """ home page request serving function """
        if(req.has_key('search_lable')):
            return self.search_msgs(req, req_id)
        if(req.has_key('msg_id') and req.has_key('new_lable')):
            return self.apply_lable(req, req_id)
        if(req.has_key('get_user_lable')):
            return self.get_user_lables(req, req_id)
        if(req.has_key('add_lable')):
            return self.add_new_lable(req, req_id)
        if(req.has_key('getService')):
            return self.get_service_list(req, req_id)
        elif (req.has_key('status')):
            return self.toggle_service(req, req_id)
        elif len(req)==5:
            return self.send_msg(req, req_id)
        else:
            return self.display_inbox(req, req_id)

    def run_watcher(self):
        new_requests = self.watch_database()
        for req in new_requests:
            request = ast.literal_eval(req[-2].replace('**', "\"").replace('[Field', "").replace(']', ""))
            pageName = request["pageName"][1]
            if(pageName == "login.html"):
                self.validate_login(request, req[0]), pageName
            elif(pageName == "reg.html"):
                self.validate_registration(request, req[0]), pageName
            elif(pageName == "home.html"):
                self.function_api(request, req[0]), pageName
