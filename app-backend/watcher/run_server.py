import select
import psycopg2
import psycopg2.extensions
import ConfigParser
import sys
from class_watcher import Watcher

conf_file = sys.argv[1]
config = ConfigParser.ConfigParser()
config.read(conf_file)
target_database = config.get('config', 'target_database')
app_database = config.get('config', 'app_database')
database_user = config.get('config', 'user')
database_password = config.get('config', 'password')
database_host = config.get('config', 'host')
database_port = config.get('config', 'port')
conn = psycopg2.connect(database=target_database, user=database_user, password=database_password, host=database_host, port=database_port)

conn.set_isolation_level(psycopg2.extensions.ISOLATION_LEVEL_AUTOCOMMIT)
curs = conn.cursor()
curs.execute("LISTEN test;")
watcher = Watcher(conf_file)
while 1:
    if select.select([conn],[],[],5) != ([],[],[]):
        conn.poll()
        while conn.notifies:
            notify = conn.notifies.pop(0)
            print "Got NOTIFY:", notify.pid, notify.channel, notify.payload
            watcher.run_watcher()
