import select
import psycopg2
import psycopg2.extensions
import ConfigParser
import sys
from class_watcher import Watcher

import pyinotify
import inotifyx

conf_file = sys.argv[1]
config = ConfigParser.ConfigParser()

def main():
    config.read(conf_file)
    stop_flag = config.get('config', 'stop')
    if(stop_flag == '1'):
        exit(0)
    sleep = config.get('config', 'sleep')
    target_database = config.get('config', 'target_database')
    polling_time = config.get('config', 'conf_sleep')
    database_user = config.get('config', 'user')
    database_password = config.get('config', 'password')
    database_host = config.get('config', 'host')
    database_port = config.get('config', 'port')

    
    conn = psycopg2.connect(database=target_database, user=database_user, password=database_password, host=database_host, port=database_port)

    conn.set_isolation_level(psycopg2.extensions.ISOLATION_LEVEL_AUTOCOMMIT)
    curs = conn.cursor()
    curs.execute("LISTEN dbchange;")
    watcher = Watcher(conf_file)

    fd = inotifyx.init()
    inotifyx.add_watch(fd, conf_file)#'/home/vishal/se_application.text')
            
    while 1:
        watcher.run_watcher()
        if(inotifyx.get_events(fd,int(sleep)) != []):
            print "here", polling_time, stop_flag
            main()
        if select.select([conn],[],[],float(polling_time)) != ([],[],[]):
            conn.poll()
            while conn.notifies:
                notify = conn.notifies.pop(0)
                print "Got NOTIFY:", notify.pid, polling_time
                
main()
