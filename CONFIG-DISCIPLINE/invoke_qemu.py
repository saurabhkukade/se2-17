import sys
import run_commad as sh


def invoke_single_qemu(qemu_image, mac, tap, name):
    '''invoke single qemu with image file and mac addr'''
    cmd = "kvm -m 128 -name "+name+ " -net nic,macaddr=" +mac+ " -net tap, script=/etc/ovs-ifup,downscript=/etc/ovs-ifdown -drive file=" +qemu_image+ ", &"
    sh.run_cmd(cmd)

