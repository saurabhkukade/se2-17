'''virtual lan setup'''
import ConfigParser
import sys
import nat_setup
import switch_setup

CONF = ConfigParser.ConfigParser()
CONF.read(sys.argv[1])
#SECTION_LIST = CONF.sections()
#SERVER_LIST = [section for section in SECTION_LIST if section != 'generic']

def setup_virt_switch ():
    '''setup virt switch as per config'''
    switch_config = dict(CONF.items('switch'))
    switch_name = switch_config['name']
    switch_ip = switch_config['ip_addr']
    switch_setup.setup(switch_name, switch_ip) 

def setup_nat():
    '''setup nat from config'''
    lan_config = dict(CONF.items('NAT'))
    in_if = lan_config['in_if']
    ex_if = lan_config['ex_if']
    public_ip = lan_config['public_ip']
    nat_script_path = lan_config['nat_script']
    nat_setup.setup_nat(in_if, ex_if, nat_script_path, public_ip)

setup_virt_switch()
setup_nat()
