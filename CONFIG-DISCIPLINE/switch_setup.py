'''Setup Virtual Switch Configuration'''
import run_commad as sh

def create_virtual_switch(switch_name):
    '''Create Virt Switch'''
    cmd = "ovs-vsctl add-br " + switch_name
    sh.run_cmd(cmd)

def assign_ip_to_virt_switch(switch_name, switch_ip):
    '''Assign ip to virtual switch'''
    cmd = "ifconfig " + switch_name + " " + switch_ip + " up"
    sh.run_cmd(cmd)

def setup(switch_name, switch_ip):
    '''main'''
    create_virtual_switch(switch_name)
    assign_ip_to_virt_switch(switch_name, switch_ip)

#main()
