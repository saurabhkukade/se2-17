'''Run Shell Commnad'''
import subprocess

def run_cmd(cmd):
    '''Run Only Single command on shell'''
    print cmd
    subprocess.call(cmd, shell=True)

def run_cmds(cmd_list):
    '''Run Only list of commands on shell'''
    for cmd in cmd_list:
        print cmd
        subprocess.call(cmd, shell=True)
