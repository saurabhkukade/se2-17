'''run all servers'''
import ConfigParser
import subprocess
import sys
import run_commad as sh

CONF = ConfigParser.ConfigParser()
CONF.read(sys.argv[1])


def run_servers():
    '''Run all servers'''
    for section in CONF.sections():
        if section != 'switch' and section != 'NAT':
            print section
            server_config = dict(CONF.items(section))
            qemu_image = server_config['img_file']
            mac_addr = server_config['mac_addr']
            tap = server_config['tap']
            cmd = 'sudo kvm -m 128 -name '+section+' -net nic,macaddr='+mac_addr+' -net tap,ifname='+tap+',script=/etc/ovs-ifup,downscript=/etc/ovs-ifdown  '+qemu_image+' &'
            sh.run_cmd(cmd)

run_servers()
