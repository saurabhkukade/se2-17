'''Setup Passwordless ssh'''

import subprocess
import run_commad as sh
import sys

def setup_passless_ssh(username, ip_addr):
    cmds = []
    cmds.append("ssh-keygen -t rsa")
    cmds.append("ssh " +username+ "@" +ip_addr+ " mkdir -p .ssh")
    cmds.append("cat ~/.ssh/id_rsa.pub | ssh " +username+ "@" +ip_addr+ " 'cat >> .ssh/authorized_keys'")
    cmds.append("ssh " +username+ "@" +ip_addr+ " 'chmod 700 .ssh; chmod 640 .ssh/authorized_keys'")
    cmds.append("ssh "+username+ "@" +ip_addr)
    sh.run_cmds(cmds)

setup_passless_ssh(sys.argv[1], sys.argv[2])
