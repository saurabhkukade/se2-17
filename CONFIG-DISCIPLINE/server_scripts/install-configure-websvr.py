"""Script install packages on webserver"""
#!/usr/bin/python
import os
import subprocess

def run_cmd(command):
    '''Run commands on shell, one by one'''
    print command
    subprocess.call(command, shell=True)



def create_command():
    """create commands for installing package on the debian"""
    lst = ["apache2","libapache2-mod-python","sqlite3"]
    for package in lst:
        cmd = "sudo apt-get install %s"%(package)
        run_cmd(cmd)

create_command()
#create_generic_linux_disk_img()
#replicate_linux_image("qemu_images/generic_linux.img", "qemu_images", "websrv.img")
# IMG_PATH = create_blank_disk_image(sys.argv[1], sys.argv[2], sys.argv[3])
# install_minimal_debian(IMG_PATH, sys.argv[4])
