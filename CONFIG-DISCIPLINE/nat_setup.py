'''Setup Nat'''
import run_commad as sh


def setup_nat(internal_if, external_if, nat_script_path, pub_ip):
    '''setup nat between internal interface and exteranl
    interface using nat shell script'''
    cmd = "sh " + nat_script_path + " " + internal_if + " " + external_if + " " + pub_ip
    sh.run_cmd(cmd)
