rm /var/www/html/web-backend
rm $(pwd)/web-backend/process_configuration.xml

rm $(pwd)/app-backend/databases/*.db
ln -s $(pwd)/web-backend /var/www/html/
ln -s $(pwd)/conf/process_configuration.xml web-backend/

chmod 777 $(pwd)/data/input-files;
chmod 777 $(pwd)/data/.processed_files;
#chown -R vishal:www-data $(pwd)/data/input-files;

service apache2 restart
