var handle_fuctions = {
    "login.html": handle_login,
    "reg.html": handle_reg,
    "home.html": handle_home_req
};

var login_response = {
    "success": "You are logged in!",
    "Invalid": "username or password is incorrect!",
    "timeout": "Server is not respondig!"
};
var lable_id_list = {};

function handle_response(request_json, response) {
    if (request_json['pageName'] == 'home.html') {
        handle_fuctions[request_json['pageName']](response, request_json);
    }else if (request_json['pageName'] == 'lable.html'){

    }else {
        handle_fuctions[request_json['pageName']](response, request_json['user_name']);
    }
}


function handle_reg(response, user_name) {
    if (response == 'success') {
        show_status("You are successfuly registered!")
        window.location.href = "login.html";
    } else {
        show_status(extract_key(response) + " is already registered!");
    }
}


function get_user_lable(){

    var data = {};
    data["pageName"] = pageName;
    data["sender_id"] = session_id;
    data["get_user_lable"] = "get_user_lable";
    excecute_service(data,true);
}
function set_msg_lable(event){
    alert(event.value());
}
function handle_home_req(response, request_json) {
    //show_status(request_json["pageName"]);
    console.log(response.split("!")[1]);
    if (response.split('!')[0] == 'lable_inbox'){
      display_inbox(response.split("!")[1])
    }
    if (response.split('!')[0] == 'services'){
        service_list = response.split('!')[1].split("#");
        user_service_list = response.split('!')[2].split("#");
        if($.inArray('lable', user_service_list) != -1){
            document.getElementById("set_leble").style.visibility="visible";
            get_user_lable();
        }
        return;

    }
    if(response.split('!')[0] == "user_lable_list"){
        var json_str = response.split('!')[1].replace(/#/g,'"');
        json_str = json_str.replace(/\(/g,'[').replace(/\)/g,']');
        user_lable_list = jQuery.parseJSON(json_str);
        return;
    }
    if(response.split('!')[0] == 'lable_add' && response.split('!')[1] == "failed"){
        show_status("lable alredy exist!");
    }
    else if(response.split('!')[0] == 'lable_add')  {
        show_status("lable created "+ response.split('!')[1]);
        document.getElementById("create_lable").style.visibility="hidden";
        document.getElementById("create_msg").style.visibility="visible";
        get_user_lable();
    }
    if(response.split('!')[0] == 'lable_apply' && response.split('!')[1] == "failed"){
        show_status("lable alredy exist on msg!");
    }
    else if(response.split('!')[0] == 'lable_apply')  {
        show_status("lable applied "+ response.split('!')[1]);
        document.getElementById("create_lable").style.visibility="hidden";
        get_user_lable();
    }
    if(response.split('!')[0] == "service_set"){

        alert("You are subscribed to "+response.split('!')[1]);
        window.location.href = "home.html";
    }
    if(response.split('!')[0] == "service_unset"){

        alert("You are unsubscribed from "+response.split('!')[1]);
        window.location.href = "home.html";
    }
    if (Object.keys(request_json).length == 5) {
        if (response == 'Invalid') {
            show_status("Message delivery failed, reciever is not valid");
        } else {
            show_status("Message has been sent");
        }
    }
    else {
        display_inbox(response);

    }
}

function handle_login(response, user_name) {
    show_status(login_response[response]);
    if (response == 'success') {
        sessionStorage.setItem("session", user_name);
        window.location.href = "home.html";
    }

}

function show_status(msg) {
    document.getElementById('status').getElementsByTagName('span')[0].innerHTML = msg;
}

function extract_key(response) {

    if (response == 'success' || response == 'timeout')
        return response;

    var key_pattern = /Key\s\(.+\)/i;
    var bracket_pattern = /\(.+\)/i;
    var x = response.match(key_pattern);
    var error_msg = (x[0].match(bracket_pattern))[0];
    index = error_msg.indexOf('=')
    return (error_msg.slice(0, index)).slice(1, -1);
}

function get_msg_list(data) {

    var ms = data.split("#");
    var inbox_ls = [];
    for (var i = 0; i < ms.length; i++) {
        inbox_ls.push(ms[i].split("**"));
    }
    return inbox_ls;

}

function openMsg(event){
    document.getElementById("create_msg").style.visibility="hidden";
    document.getElementById("show_msg").style.visibility="hidden";
    document.getElementById('date').innerHTML = event.childNodes[2].innerHTML;
    document.getElementById('msg_val').innerHTML = event.childNodes[1].innerHTML;
    document.getElementById('msg_sender').innerHTML = event.childNodes[0].innerHTML;
    document.getElementById("msg_view").style.visibility="visible";
}

function get_pagename() {
    var a = window.location.href,
        b = a.lastIndexOf("/");
    return a.substr(b + 1);
}

function display_inbox(data) {
    var msg_list = [];
    $('.msg').remove();
    if (data[0] == "success" && data == "") {
        alert("check bug");
    }
    if(data != "")
        msg_list = get_msg_list(data);
    for (var i = 0; i < msg_list.length; i++) {
        var time = msg_list[i][0];
        var msg = msg_list[i][1];
        var from = msg_list[i][2];
        var row = " <tr class='msg ' onclick='openMsg(this)'><td>"
            + from +"</td><td>"+ msg +"</td><td>"+ time
            + "</td></tr>";
        jQuery("#render_inbox").after(row);
    }


}


function get_service_list(){
    var data = {
        "pageName":get_pagename(),
        "getService":"service"
    };
    data["sender_id"] = session_id;
    excecute_service(data);
}

function toggle_service(event){
    var data = {};
    data["pageName"] = pageName;
    data["sender_id"] = session_id;
    data['service'] = event.id;
    data['status'] = event.innerHTML;
    excecute_service(data);
}




function toggle_input(id){
    if(id.disabled)
        id.disabled = false;
    else
        id.disabled = true;
}

function create_new_lable(){
    var lable_object = {
        'from_lable_rule' : '',
        'contains_lable_rule' : '',
        'apply_on_prev_msg' : '',
        'parent_lable' : '',
        'lable_name' : ''
    };
    var from_lable = document.getElementById("from_lable");
    var contains_lable = document.getElementById("contains_lable");
    var apply_prev = document.getElementById("lable_prev_apply");
    var lable_name = document.getElementById("lable_name_id");
    var parent_lable = document.getElementById("lable_parent_id");
    if((from_lable.disabled && contains_lable.disabled) || lable_name.value == ""
      || (from_lable.value == "" && contains_lable.value == "") ){
      show_status("Please Enter Atleast one rule for Lable");
      return;
    }
    if(!from_lable.disabled)
        lable_object['from_lable_rule'] = from_lable.value;
    if(!contains_lable.disabled)
        lable_object['contains_lable_rule'] = contains_lable.value;
    lable_object['apply_on_prev_msg'] = apply_prev.checked;
    if(!parent_lable.disabled)
        lable_object['parent_lable'] = parent_lable.value.trim();
    else
        lable_object['parent_lable'] = 'lable';
    lable_object['lable_name'] = lable_name.value;
    var mydata = "";
    $.each(lable_object, function(key,val){
        if(val=='')
            mydata+=key+"!!"+"NULL"+"#"
        else
            mydata+=key+"!!"+val+"#"
    });
    var data = {
        'pageName': get_pagename(),
        'sender_id': session_id,
        'add_lable': mydata,
        'request_add_lable': 'add_lable'
    };
    excecute_service(data);
}

function make_nested_lable(){
    var parent_lable = document.getElementById("lable_parent_id");
    toggle_input(parent_lable);
    var option_list = []
    $("#nested_lable_list").empty();
    if(!parent_lable.disabled){
        option_list = (render_lables(user_lable_list, 0,'lable', [], 0)).split(",");
    }
    var x = document.getElementById("nested_lable_list");
    for (var i = 0; i < option_list.length; i++)
        $("#nested_lable_list").append(option_list[i]);
    return;
}

function render_lables(obj, lid,key, ls, space){
    var nspace = " ".repeat(space * 3);
    var line = "<option value='" + nspace + key + "'>,";
    ls.push(line);
    lable_id_list[key] = lid;
    $.each(obj[key], function(value){
        ls += render_lables(obj, obj[key][value][0], obj[key][value][1], [], space + 1);
    });
    return ls;
}
function search_lable(){
  var to_search_lable = ($('#search_by_lable').val()).trim();
  if(to_search_lable == ''){
    show_status("Please enter lable to search");
    return;
  }
  else{
    alert("search lable "+to_search_lable);
  }
  var data = {};
  data["pageName"] = pageName;
  data["sender_id"] = session_id;
  data['search_lable'] = to_search_lable;
  data['search_lable_id'] = lable_id_list[to_search_lable];
  console.log(data);
  excecute_service(data);
  document.getElementById("create_lable").style.visibility="hidden";
  document.getElementById("create_msg").style.visibility="hidden";
    document.getElementById("show_msg").style.visibility="visible";
  //document.getElementById("show_msg").style.visibility="hidden";
}
