function validate_password(password){
	if(password.value.length < 8){
		show_status('please enter password more than 8 char');
		return false;
	}

}
function validateEmail(emailField)
{
	var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
	if (reg.test(emailField.value) == false)
	{
		show_status('Invalid Email Address');
		return false;
	}
}

function validateMobile(mobField)
{
	var reg = /^([0-9]{10})/;
	if (reg.test(mobField.value) == false)
	{
		show_status('Invalid mobile number');
		return false;
	}
}
