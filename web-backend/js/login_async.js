$(document).ready(function(event) {

    $("#login_btn").click(function(e) {
        e.preventDefault();
        request_json = create_login_request();
        console.log("Login request sent")
        excecute_service(request_json);
    });

});

function create_login_request() {
    var obj = $('.login-form').serializeArray();
    var request_json = new Object()
    for (entry in obj) {
        request_json[obj[entry]['name']] = obj[entry]['value']
    }
    request_json['pageName'] = 'login.html';
    return request_json
}
