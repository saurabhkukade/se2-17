var session_id;
var pageName;
var func = "../python/target_mod_python.py";
var service_list = [];
var user_service_list = [];
var user_lable_list = [];

$(document).ready(function(event) {

    pageName = get_pagename();
    if (pageName != "login.html" && pageName != "reg.html") {
	session_id = sessionStorage.getItem("session");
	if (!session_id) {
	    alert("please login");
	    window.location.href = "login.html";
	}
    }
    if (session_id && pageName != "lable.html") {

	get_service_list();

	document.getElementById('home').getElementsByTagName('span')[0].innerHTML = "welcome " + session_id;
    }


    $("form").on("submit", function(event) {

	event.preventDefault();
	if (pageName != 'home.html') {
	    var log = $("#form").serializeArray();
	    var clean_json = {};
	    for(var i in log)
	    {
		clean_json[log[i]["name"]] = log[i]["value"];
	    }
	    clean_json["pageName"] = pageName;
	    excecute_service(clean_json);
	}

    });

    $("#register").click(function(e) {
	e.preventDefault();
	window.location.href = "reg.html";
    });

    $("#gologin").click(function(e) {
	e.preventDefault();
	window.location.href = "login.html";
    });

    $("#log_out").click(function(e) {
	e.preventDefault();
	sessionStorage.removeItem("session");
	window.location.href = "login.html";
    });

    $("#received_msg").click(function(e) {
	document.getElementById("msg_view").style.visibility="hidden";
	document.getElementById("create_msg").style.visibility="hidden";
	document.getElementById("create_lable").style.visibility="hidden";
	var data = {};
	data["pageName"] = pageName;
	data["sender_id"] = session_id;
	excecute_service(data);
	document.getElementById("show_msg").style.visibility="visible";
    });

    $("#send_msg").click(function(evnt) {
	var receiver_id = document.getElementById("receiver").value;
	if (receiver_id == "") {
	    show_status("please enter receiver");
	    return;
	} else {

	    var content = document.getElementById("msg_para").value;
	    evnt.preventDefault();
	    var data = {};
	    data["pageName"] = pageName;
	    data["receiver_id"] = receiver_id;
	    data["sender_id"] = session_id;
	    data["content"] = content;
	    excecute_service(data);
	    document.getElementById('msg_para').value = "";
	    document.getElementById('receiver').value = "";
	}
    });
    $("#compose_msg").click(function(event){
	event.preventDefault();
	document.getElementById("create_msg").style.visibility="visible";
	document.getElementById("show_msg").style.visibility="hidden";
	document.getElementById("msg_view").style.visibility="hidden";
	document.getElementById("create_lable").style.visibility="hidden";
    });

    $("#set_leble").click(function(event){
      document.getElementById("create_msg").style.visibility="hidden";
      document.getElementById("show_msg").style.visibility="hidden";
      document.getElementById("msg_view").style.visibility="hidden";
      document.getElementById("create_lable").style.visibility="visible";
    });

  //   $("#save_lable").click(function(event){
	// var lable_name = $("#lable_name").val();
	// if(lable_name == ""){
	//     show_status("Please enter or select lable");
	//     return;
	// }
	// if($.inArray(lable_name, user_lable_list) == -1){
	//     var data = {
	// 	"pageName" : get_pagename(),
	// 	"sender_id" : session_id,
	// 	"new_lable" : lable_name
  //
	//     };
	//     excecute_service(data);
	// }
	// else{
	//     if(document.getElementById("msg_view").style.visibility== "visible"){
	// 	var msg_date = document.getElementById("date").innerHTML;
	// 	alert(msg_date+"hie");
	// 	data = {
	// 	    "pageName" : get_pagename(),
	// 	    "sender_id" : session_id,
	// 	    "new_lable" : lable_name,
	// 	    "msg_id" : msg_date
	// 	};
	// 	console.log(data);
	// 	excecute_service(data);
	//     }
	//     if(document.getElementById("show_msg").style.visibility== "visible"){
	// 	alert("search");
	// 	data = {
	// 	    "pageName" : get_pagename(),
	// 	    "sender_id" : session_id,
	// 	    "search_lable" : lable_name
	// 	};
	// 	excecute_service(data);
	// 	document.getElementById("show_msg").style.visibility="visible";
  //
	//     }
	//     else{
	// 	show_status("Please select msg first to apply lable.")
	//     }
	// }
  //   });
    $("#service_btn").click(function(event){
	     event.preventDefault();
	      document.getElementById("create_msg").style.visibility="hidden";
	       document.getElementById("show_msg").style.visibility="hidden";
	document.getElementById("msg_view").style.visibility="hidden";
	document.getElementById("create_lable").style.visibility="hidden";

	//var service_list = ['Lable', 'Test_Case'];
	console.log(service_list);
  var row_str = "";

	for (var i = 0; i < service_list.length; i++){
	    if($.inArray(service_list[i], user_service_list) != -1)
		var status = "Unsubscribe";
	    else
		var status = "Subscribe";

	    var btn = '<button id="'+ service_list[i] +'" onclick=toggle_service(this) class="btn btn-success">'+ status +'</button>';
	    row_str += '<p class="div-msg">'+ service_list[i] + btn+'</p>';
	}
	document.getElementById("service_indiv").innerHTML = row_str;
	document.getElementById("service_div").style.visibility="visible";
   });
    $('#lable_parent_id').change(function(){
	     $('#lable_parent_id').val($('#lable_parent_id').val())
    });
  $('#search_by_lable').focus(function(){
    make_nested_lable();
  });
});
