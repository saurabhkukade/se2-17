import psycopg2
import ConfigParser
import time
import logging
def read_config(conf_file):
        global database_name
	global database_user
	global database_password
	global database_host
	global database_port
        config = ConfigParser.ConfigParser()
	config.read(conf_file)
	database_name = config.get('config', 'database')
        database_user = config.get('config', 'user')
	database_password = config.get('config', 'password')
	database_host = config.get('config', 'host')
	database_port = config.get('config', 'port')
	log_file_name = config.get('config','log_file')
	logging.basicConfig(filename= log_file_name, level=logging.DEBUG, format='%(asctime)s : %(levelname)s : %(message)s')
def delete_served_request():
    try:
        con = get_database_connection()
        cmd = "delete from intermediate where request_id in (select request_id from intermediate where response is not null);"
        cur = con.cursor()
        cur.execute(cmd)
        con.commit()
        con.close()
    except Exception as e:
        con.close()


def get_database_connection():
	"""Returns database connection"""
	try:
		con = psycopg2.connect(database=database_name, user=database_user, password=database_password, host=database_host, port=database_port)
		return con
	except Exception as e:
		return e,"connection"," failed"
        
def convert_into_string(data):
    data = data.replace("'",'**')
    return data

def make_req(req):
    ''''''
    request_id = time.ctime()
    
    request = convert_into_string(str(req.form))
    try:
        con = get_database_connection()
        if req.form['pageName'] == "home.html":
            user_name = req.form['sender_id']
        else:
            user_name = req.form['user_name']

        cmd = "insert into intermediate (request_id,session_id,request) values ('"+request_id+"','"+user_name+"','"+request+"');"
        cur = con.cursor()
        cur.execute(cmd)
        con.commit()
        con.close()
        return request_id
    except Exception as e:
        con.close()
        return e
    return "python_errpr"


def get_response_asych(req):
	read_config(req.form['config'])
	try:
		con = get_database_connection()
		cmd = "select response from intermediate where request_id = '"+req.form['request_id']+"';"
		cur = con.cursor()
		cur.execute(cmd)
		res = cur.fetchall()
		con.close()
		if (str(res[0][0]) == 'None'):
			return req.form['request_id']
	except Exception as e:
		con.close()
		return e
	return str(res[0][0])

def make_request_asynch(req):
	read_config(req.form['config'])
	request_id = make_req(req)
	return request_id

def render_res(res):
    lo = 0
    up = 4
    ls = []
    while(up < len(res)):
        ls.append(res[lo:up])
        lo = up
        up = up + 4
    return ls


def show_inbox(req):

    request_id = make_req(req)
    if(request_id == 0):
        return "Invalid"
    res = get_response(request_id,0)
    #res = (res.replace("**","'"))
    return res
    return render_res(res.split(","))
    #return req.form
